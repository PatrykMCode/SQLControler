import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.sql.*;
import java.util.ArrayList;

//class for display and for manager sql data base
public class Display implements ActionListener {

    static JFrame window;
    static JTextField address, username, password, commnadSQL;
    static JLabel status, infoAddress, infoUsername, infoPassword;
    static JTextArea result;
    static JButton loginButton, submitCommandButton, clearButton, logoutButton, exitButton, reconnectButton, helpButton;
    static String title, addressString, usernameString, passwordString, commandSQLString, resultString;
    static int width, height;
    public static boolean isHelpOpen;

    static ArrayList resultList = new ArrayList();

    static Connection connection;

    public Display(String title, int w, int h) {
        window = new JFrame(title);
        address = new JTextField();
        username = new JTextField();
        password = new JTextField();
        infoAddress = new JLabel();
        infoUsername = new JLabel();
        infoPassword = new JLabel();
        loginButton = new JButton("Login");
        reconnectButton = new JButton("Reconnect");
        exitButton = new JButton("Exit");
        status = new JLabel();

        commnadSQL = new JTextField();
        submitCommandButton = new JButton("Submit");
        result = new JTextArea();
        clearButton = new JButton("Clear");
        logoutButton = new JButton("Logout");
        helpButton = new JButton("Help");
        this.title = title;
        this.width = w;
        this.height = h;

        isHelpOpen = false;

        if(connection == null){
            loginButton.setBackground(Color.red);
            status.setText("Logout");
            status.setForeground(Color.red);
        }

        createDisplay();
    }

    void createDisplay(){
        window.setSize(width, height);
        window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        window.setResizable(false);
        window.setVisible(true);
        window.setLayout(null);

        address.setBounds(0, 0, 100, 30);

        username.setBounds(0, 60, 100, 30);

        password.setBounds(0, 120, 100, 30);

        infoAddress.setBounds(110, 0, 100, 30);
        infoAddress.setText("address");

        infoUsername.setBounds(110, 60, 100, 30);
        infoUsername.setText("username");

        infoPassword.setBounds(110, 120, 100, 30);
        infoPassword.setText("password");

        loginButton.setBounds(0, 180, 100, 30);
        loginButton.addActionListener(this);

        exitButton.setBounds(110, 180, 100, 30);
        exitButton.addActionListener(this);

        status.setBounds(0, 230, 100, 40);

        commnadSQL.setBounds(400, 0, 320, 30);

        submitCommandButton.setBounds(400, 60, 100, 30);
        submitCommandButton.addActionListener(this);

        result.setBounds(400, 100, 100, 200);

        clearButton.setBounds(510, 60, 100, 30);
        clearButton.addActionListener(this);

        logoutButton.setBounds(620, 60, 100, 30);
        logoutButton.addActionListener(this);

        reconnectButton.setBounds(0, 210, 100, 30);
        reconnectButton.addActionListener(this);

        helpButton.setBounds(730, 60, 100, 30);
        helpButton.addActionListener(this);

        window.add(address);
        window.add(username);
        window.add(password);
        window.add(infoAddress);
        window.add(infoUsername);
        window.add(infoPassword);
        window.add(loginButton);
        window.add(exitButton);
        window.add(status);
    }

    static void login(){
        System.out.print("Login");
        addressString = address.getText();
        usernameString = username.getText();
        passwordString = password.getText();
        System.out.println("a: " + addressString + " u: " + usernameString + " p: " + passwordString);
        createConnect();
    }
    static void createConnect(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + addressString, usernameString, passwordString);
            //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/demo", "root", "Programming12");
            if(connection != null) {
                loginButton.setBackground(Color.green);
                window.setSize(800, 600);
                window.setResizable(false);
                status.setText("Login as: " + usernameString);
                status.setForeground(Color.green);
                password.setText("");

                window.add(commnadSQL);
                window.add(submitCommandButton);
                window.add(result);
                window.add(clearButton);
                window.add(logoutButton);
                window.add(reconnectButton);
                window.add(helpButton);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    static void submit() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(commandSQLString);
        while(resultSet.next()) {
            for(int i = 1; i<=4; i++){
                resultString = resultSet.getString(i);
                System.out.println(resultString);
                resultList.add(resultSet.getString(i) + "\n");
            }
            result.setText(resultList.toString());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if(source == loginButton){
            login();
        }
        if(source == exitButton){
            System.exit(1);
        }
        if(source == submitCommandButton) {
            commandSQLString = commnadSQL.getText();
            if(commandSQLString==null || commandSQLString.equals("") || commandSQLString.equals(" ")){
                JOptionPane.showMessageDialog(null, "Enter command!");
            }else{
                try {
                    submit();
                }catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }
        if(source == clearButton){
            result.setText("");
        }
        if(source == logoutButton){
            connection = null;
            window.setResizable(false);
            window.setSize(300, 400);
            if(connection == null){
                loginButton.setBackground(Color.red);
                status.setText("Logout");
                status.setForeground(Color.red);
                window.remove(reconnectButton);
            }
        }
        if(source == reconnectButton){
            login();
        }
        if(source == helpButton){
            if(isHelpOpen == false) {
                HelpDisplay hd = new HelpDisplay("HELP", 300, 400);
                if(hd.window.isActive() == true){
                    isHelpOpen = true;
                }else{
                    isHelpOpen = false;
                }
            } else{
                JOptionPane.showMessageDialog(null, "Window help is open!");
            }
        }
    }

}
